"use strict";
var total = 0,
    getIDresult = document.getElementById("result");
for (var i = 1; i <= 15; i++) {
    
    var first = Math.floor((Math.random() * 6) + 1); 
    var second = Math.floor((Math.random() * 6) + 1); 
        
    if (i == 8 || i == 13) {
         continue;
    }
    getIDresult.innerHTML += "Первая кость: " + first + " Вторая кость: " + second + "<br>";

    if (first == second) {
        getIDresult.innerHTML += "Выпал дубль. Число " + first + "<br>";
    }

   if (first < 3 && second > 4) {
        getIDresult.innerHTML += "Большой разброс между костями. Разница составляет " + (second - first) + "<br>";
    }

    total += first + second;
}

result = (total > 100) ? "Победа, вы набрали " + total + " очков" : "Вы проиграми, у вас " + total + " очков";
getIDresult.innerHTML += "<hr>" + result;