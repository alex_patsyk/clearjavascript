/*Home work #2 task 1
Создать страницу с одним внутренним скриптом и одним внешним, загрузить скрипт асинхронно.
Перед началом кода, который будет в отдельном файле, написать осмысленный комментарий.
Отформатировать следующий код. В коде допущена одна ошибка. Не хватает символа точка с запятой(;). Устраните ошибку.
(function(win){var params = {states:{url:»/»,template: «temlate.js»}}function setStates(params){if(win && !win.params){win.params = params;}}setStates();console.log(params.states.template);})(window);*/

(function(win) {
    var params = {
            states:{
                url:"/",
                template:"temlate.js"
            }
        };
    function setStates(params) {
        if (win && !win.params) {
            win.params = params;
        }
    }
    setStates();
    console.log(params.states.template);
})(window);
    