'use strict';
var formValidator = (function() {

    var DOMElements = null;
    var data = [];

    function showHideContent(action) {
        var hideClass = "hidden";
        var errMsg = DOMElements.errorMsg.classList;
        var successMsg = DOMElements.successMsg.classList;
        var table = DOMElements.table.parentNode.classList;
        var form = DOMElements.form.classList;
        switch (action) {
            case "showSuccessMsg":
                successMsg.remove(hideClass);
                break;
            case "hideSuccessMsg":
                successMsg.add(hideClass);
                break;
            case "showErrorMsg":
                errMsg.remove(hideClass);
                break;
            case "hideErrorMsg":
                errMsg.add(hideClass);
                break;
            case "showTable":
                table.remove(hideClass);
                break;
            case "hideTable":
                table.add(hideClass);
                break;
            case "showForm":
                form.remove(hideClass);
                break;
            case "hideForm":
                form.add(hideClass);
                break;
            default:
                console.log(action);
                alert("I don't know what happened!");
                break;
        }
    }

    function resetForm() {
        DOMElements.name.value = DOMElements.email.value = DOMElements.password.value = "";
        ["hideErrorMsg", "hideSuccessMsg", "hideTable", "showForm"].forEach(function (item) {
            showHideContent(item);
        })
    }

    function convertPassToStar(pass) {
        return pass.replace(/[\w \W]/g, '*');
    }

    function convertPass(event) {
        var target = event.target;
        var mode = target.getAttribute("data-password");
        var tdPassword = target.closest("td").previousElementSibling;
        var rowId = (target.getAttribute("data-id") - 1);

        if (mode == "hidden") {
            tdPassword.innerHTML = data[rowId].pass;
            target.setAttribute("data-password", "showed");
            target.textContent = "Скрыть пароль";

        } else if (mode == "showed") {
            tdPassword.innerHTML = data[rowId].passInStar;
            target.setAttribute("data-password", "hidden");
            target.textContent = "Показать пароль";
        }
    }

    function renderRow({ name, email, pass, passInStar, idRow }) {
        var row = `<tr>
                        <th scope="row">${idRow}</th>
                        <td>${name}</td>
                        <td>${email}</td>
                        <td>${passInStar}</td>
                        <td class="showHidePass"><button class="btn btn-default" data-id="${idRow}" data-password="hidden">Показать пароль</button></td>
                   </tr>`
        showHideContent("hideForm");
        DOMElements.tableContent.innerHTML += row;
    }

    function addNewTableLine() {
        var dataLength = data.push({
                                    name: DOMElements.name.value,
                                    email: DOMElements.email.value,
                                    pass: DOMElements.password.value,
                                    passInStar: convertPassToStar(DOMElements.password.value),
                                    idRow: (data.length + 1)    //пока так, с генераторами разбираюсь
                                });
        renderRow(data[dataLength - 1]);
    }

    function validateEmail(email) {
        var re = /\S+@\S+\.\S+/;
        return re.test(email);
    }

    function validate(event) {
        if (DOMElements.name.value &&
            validateEmail(DOMElements.email.value) &&
            DOMElements.password.value) {

            showHideContent("showSuccessMsg");
            showHideContent("showTable");
            addNewTableLine();
        } else {
            event.preventDefault();
            showHideContent("showErrorMsg");
        }
    }

    function initListeners() {
        DOMElements.submitBtn.addEventListener("click", validate);
        DOMElements.table.addEventListener("click", convertPass);
        DOMElements.resetBtn.addEventListener("click", resetForm);
    }

    return {
        setFormData: function(form) {
            DOMElements = form;
        },
        initValidator: function(form) {
            initListeners();
        }
    }

}());

formValidator.setFormData({
    name: document.querySelector("#inputName"),
    email: document.querySelector("#inputEmail"),
    password: document.querySelector("#inputPassword"),
    table: document.querySelector(".table"),
    tableContent: document.querySelector("#table-content"),
    submitBtn: document.querySelector("#submit"),
    resetBtn: document.querySelector("#reset"),
    successMsg: document.querySelector(".bg-success"),
    errorMsg: document.querySelector(".bg-danger"),
    form: document.querySelector("#form")
});
formValidator.initValidator();
