var main = (function() {

    var DOMElements = {
        navTabs: document.querySelector(".nav-tabs"),
        clock: document.getElementById("clock"),
        time: document.getElementById("time"),
        alarm: document.getElementById("alarm"),
        alarmMsg: document.querySelector(".bg-danger"),
        table: document.querySelector(".table"),
        tableContent: document.querySelector("#table-content"),
        deleteBtn: document.getElementById("delete"),
        setAlarm: document.getElementById("setAlarm"),
        stopwatch: document.getElementById("stopwatch"),
        timer: document.getElementById("timer")
    };


    function showHideContent(action, items) {
        items.forEach(function (item) {
            action == "show" ? item.classList.remove("hidden") : item.classList.add("hidden");
        })

    }

    function selectTab(event) {
        var target = event.target;
        var activeTabName;
        var tabList = target.closest("ul").children;
        var activeTabElem;
        for (var i = 0; i < tabList.length; i++) {
            tabList[i].classList.remove("active");
        }
        target.parentElement.classList.add("active");
        activeTabName = target.getAttribute("data-name");
        showHideContent("hide", [DOMElements.clock, DOMElements.stopwatch, DOMElements.timer]);

        switch (activeTabName) {
            case "clock":
                activeTabElem = DOMElements.clock;
                break;
            case "stopwatch":
                activeTabElem = DOMElements.stopwatch
                break;
            case "timer":
                activeTabElem = DOMElements.timer
                break;
            default:
                console.log(activeTabName);
                break;
        }
        showHideContent("show", [activeTabElem]);
    }

    function initListeners() {
        DOMElements.navTabs.addEventListener("click", selectTab);
    }

    return {
        showHideContent: showHideContent,
        formData: DOMElements,
        initListen: initListeners

    }
})();

main.initListen();
