var timeClock = (function time(moment) {
    var DOMElements = null;
    var alarmTime = null;
    var dataAlarms = [];

    function showTime() {
        var timeNow = moment(new Date());
        checkAlarm(timeNow);
        timeNow = timeNow.locale("ru").format("HH:mm:ss");
        DOMElements.time.innerHTML = timeNow;
        setTimeout(showTime, 1000);
    }

    function checkAlarm(now) {
        showHideContent("hide", [DOMElements.alarmMsg]);
        now = now.locale("ru").format("HH:mm");
        dataAlarms.forEach(function(item) {
            if (now == item.alarmTime) {
                item.status = "Сработал";
                renderTable();
                showHideContent("show", [DOMElements.alarmMsg]);
            }
        })
    }


    function renderTable() {
        DOMElements.tableContent.innerHTML = "";
        dataAlarms.forEach(function(item, index) {
            dataAlarms[index].idRow = index + 1;
            renderRow(dataAlarms[index]);
        })
        if (!dataAlarms.length) {
            showHideContent("hide", [DOMElements.table.parentNode]);
        }

    }

    function renderRow({ alarmTime, status, idRow }) {
        var row = `<tr>
                        <th scope="row">${idRow}</th>
                        <td>${alarmTime}</td>
                        <td>${status}</td>
                        <td class="delete"><button class="btn btn-default" data-id="${idRow}">Удалить</button></td>
                   </tr>`
        showHideContent("show", [DOMElements.table.parentNode]);
        DOMElements.tableContent.innerHTML += row;
    }

    function addNewTableLine() {
        var dataAlarmsLength = dataAlarms.push({
            alarmTime: DOMElements.alarm.value,
            status: "Установлен",
            idRow: (dataAlarms.length + 1)
        });

        renderRow(dataAlarms[dataAlarmsLength - 1]);
    }

    function deleteAlarm(event) {
        var target = event.target;
        var idRow = target.getAttribute("data-id");
        if (idRow) {
            dataAlarms.splice((idRow - 1), 1);
            renderTable();
        } else {
            return false;
        }
    }

    function initListeners() {
        document.addEventListener("DOMContentLoaded", showTime);
        DOMElements.setAlarm.addEventListener("click", addNewTableLine);
        DOMElements.table.addEventListener("click", deleteAlarm);
    }

    return {
        setFormData: function(form) {
            DOMElements = form;
        },
        initListen: function() {
            initListeners();
        },
        showHide: function(toggle) {
            showHideContent = toggle;
        }
    }
})(moment);

timeClock.setFormData(main.formData);
timeClock.showHide(main.showHideContent)
timeClock.initListen();
