function editFinalArray(editedArray) {
    return editedArray.map(function (item) {
        item.url         = newUrl(item.url);
        item.name        = newName(item.name);
        item.params      = newParams(item.params);
        item.description = newDescription(item.description);
        item.date        = newDate(item.date);
    })

    function newUrl(url) {return "http://" + url;}
    function newName(name) {return name[0].toLocaleUpperCase() + name.substring(1).toLowerCase();}
    function newParams(params) {return params.status + "=>" + params.progress;}
    function newDescription(description) {return description.substring(0, 15) + "...";}

    function newDate(dat){
        var tmpDate = new Date(dat);
        return fixDateString(tmpDate.getFullYear())     + "/" +
               fixDateString(tmpDate.getMonth() + 1)    + "/" +
               fixDateString(tmpDate.getDate())         + " " +
               fixDateString(tmpDate.getHours())        + ":" +
               fixDateString(tmpDate.getMinutes());
        function fixDateString(start) {return (start < 10) ? "0" + start : start;}
    }

}

function fillFirstLine(item, domElement) {
    var itemTemplate = '<div class="col-sm-4 col-xs-6">\
                            <img src="$url" alt="$name" class="img-thumbnail">\
                            <div class="info-wrapper">\
                                <div class="text-muted">$name</div>\
                                <div class="text-muted">$description</div>\
                                <div class="text-muted">$params</div>\
                                <div class="text-muted">$date</div>\
                            </div>\
                        </div>';
    var resultHTML = itemTemplate
        .replace(/\$name/gi, item.name)
        .replace("$url", item.url)
        .replace("$params", item.params)
        .replace("$description", item.description)
        .replace("$date", item.date);
    domElement.innerHTML += resultHTML;
}	
			
function fillSecondLine(item, domElement) {
    var itemTemplate = `<div class="col-sm-4 col-xs-6">\
                            <img src="${item.url}" alt="${item.name}" class="img-thumbnail">\
                            <div class="info-wrapper">\
                                <div class="text-muted">${item.name}</div>\
                                <div class="text-muted">${item.description}</div>\
                                <div class="text-muted">${item.params}</div>\
                                <div class="text-muted">${item.date}</div>\
                            </div>\
                        </div>`;
    domElement.innerHTML += itemTemplate;
}

function fillThirdLine(item, domElement) {

    var thumbnail = document.createElement("div");
    thumbnail.setAttribute("class", "col-sm-4 col-xs-6");
    thumbnail.appendChild(picture);
    thumbnail.appendChild(infowrapper);
    domElement.appendChild(thumbnail);
    
    var picture = document.createElement("img");
    picture.setAttribute("src", item.url);
    picture.setAttribute("alt", item.name);
    picture.setAttribute("class", "img-thumbnail");

    var infowrapper = document.createElement("div");
    infowrapper.setAttribute("class", "info-wrapper");

    var name = document.createElement("div");
    name.setAttribute("class", "text-muted");
    name.innerHTML = item.name;
    infowrapper.appendChild(name);
    
    function copyElement(property) {
        var newElement = name.cloneNode(true);
        newElement.innerHTML = property;
        infowrapper.appendChild(newElement);
    }
    
    copyElement(item.description);
    copyElement(item.params);
    copyElement(item.date);
}

(function main() {
    var firstLine = document.querySelector('#first-line');
    var secondLine = document.querySelector('#second-line');
    var thirdLine = document.getElementById("third-line");
    var finalArray = editFinalArray(data);
    
    finalArray.forEach(function(item, index) {
        if(index < 3) {
            fillFirstLine(data[index], firstLine)
        } else if(index < 6) {
            fillSecondLine(data[index], secondLine);
        } else if(index < 9) {
            fillThirdLine(data[index], thirdLine);
        }
    })
})();