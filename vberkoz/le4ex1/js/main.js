﻿var total = 0;
var result = document.getElementById("result");

for (var i = 0; i < 15; i++) {
    
    if(i == 8 || i == 13){continue;}
    
    var first = Math.floor((Math.random() * 6) + 1);
    var second = Math.floor((Math.random() * 6) + 1);
    
    result.innerHTML += "Первая кость: " + first + " Вторая кость: " + second + "<br/>";
    
    if(first == second) result.innerHTML += "Выпал дубль. Число " + first + "<br/>";
    if(first < 3 && second > 4) result.innerHTML += "Большой разброс между костями. Разница составляет " + (second - first) + "<br/>";
    if(second < 3 && first > 4) result.innerHTML += "Большой разброс между костями. Разница составляет " + (first - second) + "<br/>";
    
    total += first + second;
    
}

result.innerHTML += (total > 100)? "Победа, вы набрали очков: " + total: "Вы проиграли, у вас очков: " + total;
