var btn = document.getElementById("play");
var player1 = document.getElementById("player1");
var player2 = document.getElementById("player2");
var resultElement = document.getElementById("result");

function getPlayerResult() {
   return Math.floor((Math.random() * 3) + 1);
}

function getNameById(name) {
    switch (name){
        case 1:
            return "камень";
            break;
        case 2:
            return "ножницы";
            break;
        case 3:
            return "бумага";
            break;
    }
 
}

function determineWinner(result1, result2){
    if (result1 == result2){
        return 0;
    } else if (result1 == 1 && result2 == 2){
        return 1;
    } else if (result1 == 2 && result2 == 1){
        return 2
    } else if (result1 == 3 && result2 == 1){
        return 1;
    } else if (result1 == 3 && result2 == 2){
        return 2;
    } else if (result1 == 1 && result2 == 3){
        return 2;
    } else if (result1 == 2 && result2 == 3){
        return 1;
    }
}

function printResult(result){
    switch (result) {
        case 1:
           return "Выиграл первый игрок";
           break;    
        case 2:
           return "Выиграл второй игрок";
           break;
        case 0:
           return "Ничья";
           break;
    }   
}


function runGame() {
    
    var first = getPlayerResult();
    var second = getPlayerResult();
        
    player1.innerHTML = getNameById(first);
    player2.innerHTML = getNameById(second);
    
    resultElement.innerHTML = printResult(determineWinner(first, second));
}



btn.addEventListener("click", runGame);

