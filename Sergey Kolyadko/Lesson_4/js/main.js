


var total = 0;
var resultElement = document.getElementById("result");

for (var i = 0; i < 15; i++){
	if(i == 8 || i == 13) { continue};
	
	var first = Math.floor((Math.random() * 6) + 1); 
	var second = Math.floor((Math.random() * 6) + 1);
	
	resultElement.innerHTML += "Первая кость: " + first + " Вторая кость: " + second + "<br>";
	
	if(first==second){
		var number = second;
		resultElement.innerHTML += "Выпал дубль. Число " + number + " <br>";
	}
	
	if(first <= 3 && second >= 4){
		var interval = second - first;
		resultElement.innerHTML += "Большой разброс между костями. Разница составляет " + interval + " <br>";
	}
	
	total += first + second;
}

resultElement.innerHTML += (total > 100) ? "<br/> <b>Победа, вы набрали </b>"  +  "<span style='color:green; font-size:24px; font-weight: bold'>" + total + "</span>" + " <b>очков</b>" :"<br/> <b>Вы проиграли, у вас </b>"  +  "<span style='color:red; font-size:24px; font-weight: bold'>" + total + "</span>" + " <b>очков</b>";


