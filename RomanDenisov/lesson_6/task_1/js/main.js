"use strict";

let btn = document.getElementById("play");


let createNewArray = (arr, count) => {
    let newArr = [];
    arr.forEach((item, index) => {
        if(count <= index){
            newArr.push({
                    url : item.url,
                    name : item.name,
                    params : item.params,
                    description : item.description,
                    date : item.date
            })
        }
    });
    return newArr;
};

let transformArray = (arr) => {
    return arr.map((item) => {
       return {
            url:`http://${item.url}`,
            name: `${item.name.charAt(0).toUpperCase()}${item.name.slice(1).toLowerCase()}`,
            params: `${item.params.status} => ${item.params.progress}`,
            description: `${item.description.substring(0, 15)}...`,
            date: timeStamps(item.date)
       }
    });
};

let timeStamps = (timestamp) => {
    let tempTime = new Date(timestamp);
    let year = tempTime.getFullYear(),
        month = tempTime.getMonth() + 1,
        day = tempTime.getDate(),
        hours = tempTime.getHours(),
        minutes = tempTime.getMinutes();

    if (month < 10) {
        month = `0${month}`;
    }
    if (day < 10) {
        day = `0${day}`;
    }
    if (hours < 10) {
        hours = `0${hours}`;
    }
    if (minutes < 10) {
        minutes = `0${minutes}`;
    }

    return `${year}/${month}/${day} ${hours}:${minutes}`
};

let printResultArray = (arr) => {
    console.log(arr);
};

let transform = () => {
    try{
        let numberForChange = prompt("Сколько элементов надо отбросить ?");
        if(numberForChange > 9) throw new SyntaxError("Данные некорректны, " +
            "Введите допустимое число от 0 до 9");
        let newArr = createNewArray(data, numberForChange);
        let changedArray = transformArray(newArr);
        printResultArray(changedArray);

    } catch(e) {
        alert(`${e.message} !`)
    }
};

btn.addEventListener("click", transform);