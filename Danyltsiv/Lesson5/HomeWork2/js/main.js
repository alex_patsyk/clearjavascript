/*У вас есть 2 игрока которые играют в игру. У каждого может выпасть камень, ножницы или бумага.
На самом деле у вас есть функция (getPlayerResult) которая возвращает случайные числа от 1 до 3
1 — камень
2 — ножницы
3 — бумага
В заготовке реализован следующий функционал.
По нажатии на кнопку получаем случайное число и выводим его в соответствующий div элемент.
1. Вместо того чтоб выводить на екран случайное число как в примере вам необходимо добавить функцию (getNameById) которая будет принимать это число и возвращать слово «камень», «ножницы», или «бумага» согласно словарю указанному выше.
2. На экран вывести полученную текстовую строку для каждого из игроков.
3. Написать функцию (determineWinner), которая будет принимать два числа, предварительно полученные в функции getPlayerResult и принимать решение который из игроков выиграл.
4. Результатом выполнения функции determineWinner должно быть число, номер игрока, который выиграл. То есть эта функция должна возвращать номер игрока который выиграл
5. Функция printResult должна принять номер игрока, который выиграл и напечатать в div Id result текстовое сообщение типа: «выиграл первый игрок» номер игрока надо вывести словами.
*/
var btn = document.getElementById("play");
var player1 = document.getElementById("player1");
var player2 = document.getElementById("player2");
var myResult = document.getElementById("result");

function getPlayerResult() {
    return Math.floor((Math.random() * 3) + 1);
}

function showResult(text) {
	myResult.innerHTML = text;
}

function getNameById(playerHand){
	var hand;
	switch (playerHand) {
		case 1: 
			hand = "<div class='img1'>" + " " + "</div>";
			break;
		case 2:
			hand = "<div class='img2'>" + " " + "</div>";
			break;
		case 3:
			hand = "<div class='img3'>" + " " + "</div>";
			break;
	}
	return hand;
}

function getWiner(playerFirst, playerSecond) {
	var winner;
	var battle = +(String(playerFirst) + String(playerSecond));
	switch(battle){
		case 12:
		case 23:
		case 31:
			winner = 1;
			break;
		case 21:
		case 32:
		case 13:
			winner = 2;
			break;
		case 33:
		case 22:
		case 11:
			winner = 0;
			break;
	}
	return winner;
}

function printResult(winner) {
	switch(winner) {
		case 1:
			showResult("<div class='totalresult'>" + "Вітаю, виграв 1 ігрок" + "</div>");
			break;
		case 2:
			showResult("<div class='totalresult'>" + "Вітаю, виграв 2 ігрок" + "</div>");
			break;
		case 0:
			showResult("<div class='totalresult'>" + "Ніч'я, виграла дружба" + "</div>");
			break;
	}
	
}

function runGame() {
	var playerFirst = getPlayerResult();
	var playerSecond = getPlayerResult();
    player1.innerHTML = getNameById(playerFirst);
    player2.innerHTML = getNameById(playerSecond);
	printResult(getWiner(playerFirst,playerSecond));
}
btn.addEventListener("click", runGame);
