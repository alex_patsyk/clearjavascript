﻿var display = document.getElementById("result");
var setRdNum = function () {
    return Math.floor((Math.random() * 6) + 1);
};
var print = function (display, text) {
    display.innerHTML += text;
};
var isNumEqual = function (first, second) {
    if (first == second) {
        print(display, "Выпал дубль. Число " + first + " <br>");
    }
};
var isBigDiff = function (first, second) {
    if (first < 3 && second > 4) {
        print(display, "Большой разброс между костями. Разница составляет " + (second - first) + "<br>");
    }
};
var findWinner = function (total) {
	var res = total >= 100 ? "Победа, вы набрали " + total + " очей " : "Вы проиграли, у вас " + total + " очей";
	return res;
};
function run() {
	var total = 0;
    for (var i = 0; i <= 15 ; i++) {
        if (i == 8 || i == 13) {
            continue;
        }
        var first = setRdNum();
        var second = setRdNum();
		total += first + second;
        print(display, "Первая кость: " + first + " Вторая кость: " + second + "<br>");
        isNumEqual(first, second);
        isBigDiff(first, second);
    }
	print (display, findWinner(total));
}
run();