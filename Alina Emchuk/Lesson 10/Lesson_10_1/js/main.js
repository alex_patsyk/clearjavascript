var messenger = (function() {
    var count = 0,
        firstName = "",
        secondName = "";

    function setFirstName(name) {
        firstName = "Mr. " + name;
    }

    function setSecondName(surname) {
        secondName = surname;
    }

    function getMessageText () {
        var greetingMessage = "Welcome " + firstName + " ";
            greetingMessage += secondName + ". Glad to see you ";
        return greetingMessage;
    }

    return {
        init: function(name, surname) {
                setFirstName(name);
                setSecondName(surname);
        },
        sayHello: function () {
            console.log(getMessageText() + "Count " + (count++));
        },
        resetCount: function () {
            count = 0;
            console.log("Count is 0");
        }
    }

})();
messenger.init("John", "Smith");
messenger.sayHello();
