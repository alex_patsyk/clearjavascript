var btn = document.getElementById("play");

function createNewArr(arr, count){
    var newArr = [];
    arr.forEach(function(item, index) {
        if (index < count) {
            newArr.push({
                name: item.name,
                url: item.url,
                params: item.params,
                description: item.description,
                date: item.date
            })
        }
    });
    return newArr;
}
function capitalizeFirstLetter(word){
    return word.charAt(0).toUpperCase() + word.substring(1).toLowerCase();
}
function addProtocol(adress){
    return "http://" + adress;
}
function truncateString(string){
    return string.substring(0, 15) + "...";
}
function formatDate(date){
    var tmpDate = new Date(date);
    return tmpDate.getFullYear() + "/" +
           (tmpDate.getMonth()+1) + "/" +
           tmpDate.getDate() + " " +
           tmpDate.getHours() + ":" +
           tmpDate.getMinutes();
}
function editParams(firstPar, secondPar){
    return String(firstPar) + "=>" + String(secondPar);
}
function transformeArr(arr){
    return arr.map(function(item){
        return {
            name: capitalizeFirstLetter(item.name),
            url: addProtocol(item.url),
            description: truncateString(item.description),
            date: formatDate(item.date),
            params: editParams(item.params.status, item.params.progress)
        }
    });
}
function print(text){
    console.log(text);
}

function transform() {
    var newAmount = 7;
    var newData = createNewArr(data, newAmount);
    var transformedArr = transformeArr(newData);
    print(transformedArr);
}


btn.addEventListener("click", transform);