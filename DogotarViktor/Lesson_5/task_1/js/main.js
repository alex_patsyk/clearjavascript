
function run() {
	var getTotal = 0;
	var getIDresult = document.getElementById("result");

	var getRndNumber = function () {
	    return Math.floor((Math.random() * 6) + 1);
	};

	 var print = function (getIDresult, text) {
	    getIDresult.innerHTML += text;
	};

	var isNumEqual = function (first, second) {
	    if (first == second) {
	        print(getIDresult, "Выпал дубль. Число " + first + " <br>");
	    }
	};	

	var isBigDiff = function (first, second) {
	    if (first < 3 && second > 4) {
	        print(getIDresult, "Большой разброс между костями. Разница составляет " + (second - first) + "<br>");
	    }
	};


	var isTotal = function (first, second) {
	    getTotal += first + second;
	    return getTotal;
	};

	var resultsGame = function (getTotal) {
		var result = getTotal >= 100 ? "<strong  style='font-size: 15px'>Победа, вы набрали <strong style='color:red; font-size: 25px'>" + getTotal + "</strong>  очков</strong>": "<strong  style='font-size: 15px'>Вы проиграли, у вас <strong style='color:red; font-size: 25px'>" + getTotal + "</strong> очков</strong>";

		return result;
	};


	for(var i = 1; i <= 15; i++){
		if(i == 8 || i==13){continue};

		var first = getRndNumber();
        var second = getRndNumber();

        print(getIDresult, "Первая кость: " + first + " Вторая кость: " + second + "<br>");

        isNumEqual(first, second);
        isBigDiff(first, second);
        isTotal(first, second);

        print(getIDresult, "Первая кость: " + getTotal + " Вторая кость: " + second + "<br>");
		
			
	}
	print (getIDresult, resultsGame(getTotal));
}
run();
