var btn = document.getElementById("play"),
    player1 = document.getElementById("player1"),
    player2 = document.getElementById("player2"),
    resultOutput = document.getElementById("result");

function getPlayerResult() {
    return Math.floor((Math.random() * 3) + 1);
}
function getNameById(valuePlayer){
	switch(valuePlayer){
		case 1:
	        return "камень";
	        break;
	    case 2:
	        return "ножницы";
	        break;
	    case 3:
	        return "бумага";
	        break;
	}
}


function determineWinner(firstPlayer, secondPlayer){
    if (firstPlayer == secondPlayer){
        return 0;
    } else if (firstPlayer == 1 && secondPlayer == 2 || firstPlayer == 3 && secondPlayer == 1 || firstPlayer == 2 && secondPlayer == 3){
        return 1;        
    } else if (firstPlayer == 2 && secondPlayer == 1 || firstPlayer == 3 && secondPlayer == 2 || firstPlayer == 1 && secondPlayer == 3){
        return 2
    } 
}

function printResult(gameResults){
    switch (gameResults) {
        case 1:
           return "Выиграл первый игрок";
           break;    
        case 2:
           return "Выиграл второй игрок";
           break;
        case 0:
           return "Ничья";
           break;
    }   
}



function runGame() {

	var firstPlayer = getPlayerResult();
  var secondPlayer = getPlayerResult();
   
  player1.innerHTML = getNameById(firstPlayer);
  player2.innerHTML = getNameById(secondPlayer);

	resultOutput.innerHTML = printResult(determineWinner(firstPlayer, secondPlayer));   
    
}


btn.addEventListener("click", runGame);

