var add_slide = document.querySelector('#add-slide'),
	slides_output = document.querySelector('#result'),
	numberSlides = document.querySelector('#count'),
	slide_counter = 0,
    remove_slide = document.querySelector('.btn'),
    result = transformNewArray(getNewArray(data));



function editName(name){
    return name[0].toUpperCase() + name.slice(1).toLowerCase(); 
}
function editUrl(url){
    return "http://"+url;
}
function editDescriprion(description){
    return description.slice(0, 15) + "...";
}
function editParams(params){
    return params.status + "=>" + params.progress;
}

function editDate(date){
    var tmpDate = new Date(date);
    function getCorrectDate(val){
        if(val < 10){
            return "0" + val; 
        } else {return val;}
    }
    return tmpDate.getFullYear() + "/" +
           getCorrectDate(tmpDate.getMonth() + 1)   + "/"  +
           getCorrectDate(tmpDate.getDate()) + " " +
           getCorrectDate(tmpDate.getHours()) + ":" +
           getCorrectDate(tmpDate.getMinutes());
}

function transformNewArray(newArray){
    return newArray.map(function(item, index){
        return {
            name: editName(item.name),
            url: editUrl(item.url),
            description: editDescriprion(item.description),
            date: editDate(item.date),
            params: editParams(item.params)
        };
    })
   
}

function getNewArray(arr, count){
    var newArr = [];
    arr.forEach(function (item, index){
       newArr.push({
            url: item.url,
            name: item.name,
            params: item.params,
            description: item.description,
            date: item.date
        });
    })
    return newArr;
}

function insertWithInterpolationMethod(item, output){
    var resultHTML = "";
    var itemTemplate = `<div class="col-sm-3 col-xs-6" id="data_id_${slide_counter}">\
                        <img src="${item.url}" alt="${item.name}" class="img-thumbnail">\
                        <div class="info-wrapper">\
                            <div class="text-muted">${item.name}</div>\
                            <div class="text-muted">${item.description}</div>\
                            <div class="text-muted">${item.params}</div>\
                            <div class="text-muted">${item.date}</div>\
                            <input type="button" class="btn btn-danger" value="remove" onclick="removeSlide(event, ${slide_counter});" />
                        </div>\
                    </div>`;

    output.innerHTML += itemTemplate;
}



function removeSlide(event, id_slide){
    var target = event.target,
        parent = document.getElementById('result');
    target_class = document.getElementById('data_id_'+id_slide);
    result = parent.removeChild(target_class);
    slide_counter--;
    numberSlides.innerHTML = slide_counter;
}

function addSlid(){
    if(slide_counter<result.length){
        insertWithInterpolationMethod(result[slide_counter], slides_output);
        numberSlides.innerHTML = slide_counter+1;
        slide_counter++;
    } else {
        alert('No more elements!');
    }
}

add_slide.addEventListener('click', addSlid);