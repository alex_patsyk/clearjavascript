"use_strict";
	result_block = document.getElementById('result'),
	total_block = document.getElementById('result');

//Task 1
function run(){
	var total = 0;
	for (var i = 1; i <= 15; i++) {
		var first  = getRndNumber(), 
		second = getRndNumber();
				
		printResult(result_block, i + ". Первая кость: " + "<b>" + first + "</b>" + " Вторая кость: " + "<b>" + second + "</b>" + "<br/>");
		isNumbersEqual(first, second)
		isBigDifference(first, second)	
		total += calculateTotal(first, second);
	}
	
	total_output = total >= 100 ? "Победа, вы набрали очков " + "<b>" + total +"</b>" : "Вы проиграли, у вас очков " + "<b>" + total + "</b>";
	printResult(total_block, total_output);
}

//Task 2
function getRndNumber(){
	return Math.floor((Math.random() * 6) + 1);
}

//Task 3
function printResult(print_block, output){
	print_block.innerHTML += output;
}

//Task 4
function isNumbersEqual(first, second){
	if (first == second) {
		printResult(result_block, "Выпал дубль. Число " + "<b>" + first + "</b>" + "<br/>");
  }
}

//Task 5
function isBigDifference(first, second){
	if (first < 3 && second > 4) {
		printResult(result_block, "Большой разброс между костями. Разница составляет " + "<b>" + (second - first) + "</b>" + "<br/>");
  }
}

//Task 6
function calculateTotal(first, second){
	return first+second;
}

run();