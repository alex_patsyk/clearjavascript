var btn        = document.getElementById("play"),
    player1    = document.getElementById("player1"),
    player2    = document.getElementById("player2"),
    result_div = document.getElementById("result");

function getPlayerResult() {
    return Math.floor((Math.random() * 3) + 1);
}

//Task 1
function getNameByNumber(game_code) {
    switch (game_code) {
        case 1:
            return 'Камень';
            break;
        case 2:
            return 'Ножницы';
            break;
        case 3:
            return 'Бумага';
            break;
    }
}


function determineWinner(first, second) {
    if (first == second) {
        return 0;
    } else if ( first == 1 && second == 2 ) {
        return 1;
    } else if ( first == 2 && second == 1 ) {
        return 2;
    } else if ( first == 1 && second == 3 ) {
        return 2;
    } else if ( first == 3 && second == 1 ) {
        return 1;
    } else if ( first == 2 && second == 3 ) {
        return 1;
    } else if ( first == 3 && second == 2 ) {
        return 2;
    }
}

function printResult(game_code){
    var output = 0;
    if (game_code == 0) {
        output = "Ничья";
    } else if (game_code == 1) {
        output = "Выиграл первый игрок";
    } else {
        output = "Выиграл второй игрок";
    }
    result_div.innerHTML = output;
}


function runGame() {
    var first  = getPlayerResult();
        second = getPlayerResult();
    player1.innerHTML = getNameByNumber(first);
    player2.innerHTML = getNameByNumber(second);
    printResult( determineWinner(first,second) );
}



btn.addEventListener("click", runGame);