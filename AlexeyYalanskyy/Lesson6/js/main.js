var btn = document.getElementById("play");

function createNewArray(arr, count) {
	var newArr = [];
	arr.forEach(function(item, index) {
		if (index <= count - 1) {
			newArr.push({
		 		url 				: item.url,
		 		name 				: item.name,
		 		params 			: item.params,
		 		description : item.description,
		 		date 				: item.date
		 	});
		}
	})
	return newArr;
}	
function modifyName(Name) {
	return Name[0] + Name.substring(1).toLowerCase()
}
function modifyUrl(url) {
	return "http://" + url;
}
function modifyDescription(description) {
	return description.slice(0, 15) + "...";
}
function modifyDate(date) {
	//return local = moment(date).format("YYYY/MM/DD HH:mm");
	var firstDate = new Date(date);
	function createNewDate(argument) {
		if(argument < 10) {
			return "0" + argument;
		} else {
			return argument;
		}
	}
	return firstDate.getFullYear() + "/" +
			createNewDate(firstDate.getMonth()+1) + "/" +
			createNewDate(firstDate.getDate()) + " " +
			createNewDate(firstDate.getHours()) + ":" +
			createNewDate(firstDate.getMinutes());

}
function modifyParams(params) {
	return params.status + "=>" + params.progress
}

function editNewArr(arr) {
	return arr.map(function (item, index) {
		return {
			name 				: modifyName(item.name),
			url 				: modifyUrl(item.url),
			description : modifyDescription(item.description),
			date 				: modifyDate(item.date),
			params 			: modifyParams(item.params)
		};
	})
}
function print(printText) {
	console.log(printText);
}

function transform() {
	var count = 7;
	var result = editNewArr(createNewArray(data, count));
	print(result);
}

btn.addEventListener("click", transform);