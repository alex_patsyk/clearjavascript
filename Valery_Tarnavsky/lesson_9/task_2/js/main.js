var clock = (function(){
    var elems = null;
    var alarmsList = [];

    function makeClock(){
        var nowDateMilliseconds = Date.now();
        nowDateMilliseconds += 1000;
        runClock(nowDateMilliseconds);
    }

    function runClock(date) {
        var tmpDate = new Date(date);
        var fixDate = {
            hours: tmpDate.getHours(),
            minutes: tmpDate.getMinutes(),
            seconds: tmpDate.getSeconds()
        };
        for (var k in fixDate) {
            fixDate[k] = fixDate[k] < 10  ? ('0'+ fixDate[k]) : fixDate[k];
        }
        elems.clock.innerHTML = fixDate.hours + ":" + fixDate.minutes + ":" + fixDate.seconds;

        if (fixDate.seconds == "00"){
            compare(fixDate.hours, fixDate.minutes);
        }
    }

    setInterval(makeClock, 1000);

    function validate(event){
        event.preventDefault();
        var regExp =  /^([01]?[0-9]|2[0-3]):[0-5][0-9]$/;
        var enterTest = regExp.test(elems.setAlarm.value);
        if(enterTest){
            createAlarmsList();
        }
    }

    function createAlarmsList(){
        alarmsList.push(elems.setAlarm.value);
        createAlarmsTable(alarmsList[alarmsList.length-1], alarmsList.indexOf(elems.setAlarm.value));
        toggleTable(true);
    }

    function toggleTable(status){
        elems.tableContainer.classList.toggle("hidden", !status);
    }

    function createAlarmsTable(item, index){
        var rowNum = elems.tableContent.rows.length + 1;
        var row = '<tr>\
                        <td class="rowNumber">'+ rowNum +'</td>\
                        <td>'+ item +'</td>\
                        <td><a class="btn btn-danger btn-sm deleteBtn" data-target="'+index+'">Удалить будильник</a></td>\
                   </tr>';
        elems.tableContent.innerHTML += row;
    }

    function deleteAlarm(){
        var target = event.target;
        if (target.tagName != 'A') {return}
        var trElem = target.closest("TR");
        var elemNumber = target.getAttribute("data-target");
        delete alarmsList[elemNumber];
        trElem.remove();
        recalculateRowNumber();
    }

    function recalculateRowNumber(){
        var recalculate = 1;
        for (var i = 0; i < elems.rowNumbers.length; i++){
            var item = elems.rowNumbers[i];
            item.innerHTML = recalculate++;
        }
    }

    function compare(hours, minutes){
        alarmsList.forEach(function(item){
            var alarmHours = item.split(":")[0];
            var alarmMinutes = item.split(":")[1];
            if(alarmHours == hours && alarmMinutes == minutes){
                toggleMsg(true);
                setTimeout(function(){toggleMsg(false)}, 10000);
            }
        });
    }

    function toggleMsg(status){
        elems.errorMsg.classList.toggle("hidden", !status);
    }

    function initListeners() {
        elems.submitBtn.addEventListener("click", validate);
        elems.tableContent.addEventListener("click", deleteAlarm);
    }

    return {
        setClockData : function(clockData){
            elems = clockData;
        },
        initValidator: function(){
            initListeners();
        }
    }
})();

clock.setClockData({
    successMsg     : document.querySelector(".bg-success"),
    errorMsg       : document.querySelector(".bg-danger"),
    formContainer  : document.querySelector("#form"),
    setAlarm       : document.querySelector("#form input"),
    clock          : document.querySelector("#clock"),
    tableContainer : document.querySelector("#tableContainer"),
    tableContent   : document.querySelector("#tableContent"),
    rowNumbers     : document.getElementsByClassName("rowNumber"),
    submitBtn      : document.querySelector("#submit"),
    deleteBtn      : document.querySelectorAll(".deleteBtn")
});
clock.initValidator();