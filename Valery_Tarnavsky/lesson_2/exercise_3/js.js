var myVar;
console.log(myVar); // undefined

myVar = true;
console.log(myVar); // true

myVar = 123;
console.log(myVar); // 123

myVar = "same string";
console.log(myVar); // same string

myVar = null;
console.log(myVar); // null

showType = typeof(myVar);
console.log(showType); // object

