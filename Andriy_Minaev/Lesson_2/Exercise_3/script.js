var a;
console.log(a); // undefined

var a = 1;
console.log(a); // 1

var a = true;
console.log(a); // true

var a = "yes";
console.log(a); // yes

var a = null;
console.log(a); // null

var typeShow = (typeof a);
console.log(typeShow); //object

